{ pkgs ? import <nixpkgs> {}}:
let

mc = pkgs.callPackage ../mailcow.nix {};

in

pkgs.stdenv.mkDerivation rec {
  name = "mailcow";
  phases = "installPhase";
  installPhase = ''
    mkdir -p $out/bin
    cp ${mc.runMailCow.command}/bin/runMailCow $out/bin/runMailCow
    cp ${mc.configureMailCow.command}/bin/configureMailCow $out/bin/configureMailCow
    cp ${mc.updateMailCow.command}/bin/updateMailCow $out/bin/updateMailCow
  '';
}



