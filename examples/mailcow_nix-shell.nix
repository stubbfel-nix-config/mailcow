{ pkgs ? import <nixpkgs> {}}:
let

mc = pkgs.callPackage ../mailcow.nix {};

in

pkgs.mkShell {
    name = "mailcow-shell";
    buildInputs = mc.runMailCow.paths ++ mc.configureMailCow.paths ++ [
      mc.runMailCow.command
      mc.configureMailCow.command
      mc.updateMailCow.command
      pkgs.nano
    ];
}

