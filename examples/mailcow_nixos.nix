{ config, pkgs, ...}:

let
  mc = pkgs.callPackage ../mailcow.nix {
    ptr = "server.of.devel-nixos.org";
    httpPort = "7780";
    httpBind = "127.0.0.1";
    httpsPort = "7443";
    httpsBind = "127.0.0.1";
    skipMailCowAcme = true;
    nginxServerAliases = [ "mail.devel-nixos.org" ];
  };
in
{

  networking.firewall.allowedTCPPorts = [80 443];

  services.nginx = {
    enable = true;
    appendHttpConfig = ''
      server_names_hash_bucket_size 64;
    '';
    sslProtocols = "TLSv1 TLSv1.1 TLSv1.2";
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = false;
    virtualHosts."mailcow.*" = mc.nginx.virtualHosts.mailCow;
  };

  systemd = {
    services = {
      mailcow = mc.systemd.services.mailcow;
      mailcow-update = mc.systemd.services.mailcow-update;
    };
    timers.mailcow-update = mc.systemd.timers.mailcow-update;
  };

  security.acme = {
    acceptTerms = true;
    email = "acme@devel-nixos.org";
    certs."${mc.acme.certName}".postRun = mc.acme.postRun;
  };
}
