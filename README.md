# MailCow

## Parameter

```nix
{
  pkgs, # parameter for nixpkgs set
  stateDir, # install target folder
  ptr, # PTR Resource Record https://www.cloudns.net/wiki/article/40/, will be also use as server name for the nginx setting
  timeZone, # time zone
  httpPort, # http port for mailcow website
  httpBind, # http bind, e.g if you use a reverse proxy then you want that mailcow website us only listen to like 127.0.0.1
  httpsPort, # https port for mailcow website
  httpsBind, # https bind, e.g if you use a reverse proxy then you want that mailcow website us only listen to like 127.0.0.1
  skipMailCowAcme, # skip the acme container of mailcow e.g. you use a own one
  nginxEnableACME, # enable acme for nginx setting
  nginxForceSSL, # force ssl usage for nginx setting
  nginxIsDefault, # set mailcow virtual host as default host
  nginxServerAliases, # list of additional domains which would handle by the mail cow virtual host
  gitUrl ? "https://github.com/mailcow/mailcow-dockerized.git", # mailcow clone url
  gitRef ? "master" # git ref for init setup
}
```

## Return value

```nix
{
  runMailCow = {  # shell script which run the mail cow
    command, # the run script
    paths # build path for the run script
  },
  configureMailCow = { # shell script which configure the mail cow
    command, # the configure script
    paths # build path for the configure script
  },
  updateMailCow = { # shell script which update the mail cow
    command, # the update script
    paths # build path for the update script
  },
  systemd = {
    services = {
      mailcow, # systemd mail cow service
      mailcow-update # systemd mail cow update service
    },
    timers.mailcow-update # systemd timer which invoke the mail cow update service weekly
  },
  nginx.virtualHosts.mailCow, # ngnix virtual host setting for the mail cow service
  acme = {
    certName, # name of the acme cert 
    postRun # copy the acme script to the mail cow cert path 
  }
}
```

## Example

### shell

```sh
nix-shell examples/mailcow_nix-shell.nix --run configureMailCow
nix-shell examples/mailcow_nix-shell.nix --run runMailCow
nix-shell examples/mailcow_nix-shell.nix --run updateMailCow
```
for details see `examples/mailcow_nix-shell.nix`

### build

```sh
nix-build examples/mailcow_nix-build.nix
result/bin/configureMailCow
result/bin/runMailCow
result/bin/updateMailCow
```
for details see `examples/mailcow_nix-build.nix`

### nixos

see `examples/mailcow_nixos.nix`