# nix-build  --expr 'with import <nixpkgs> {}; callPackage ./mailcow.nix {}'
{
  pkgs ? import <nixpkgs> {},
  stateDir ? "/opt/mailcow-dockerized",
  ptr ? "mail.example.org",
  timeZone ? "Europe/Berlin",
  httpPort ? "80",
  httpBind ? "0.0.0.0",
  httpsPort ? "443",
  httpsBind ? "0.0.0.0",
  skipMailCowAcme ? false,
  nginxEnableACME ? true,
  nginxForceSSL ? true,
  nginxIsDefault ? true,
  nginxServerAliases ? []
  gitUrl ? "https://github.com/mailcow/mailcow-dockerized.git",
  gitRef ? "master"
}:
let

configureMailCowPaths = [
  pkgs.gnused
];

runMailCowPaths =  configureMailCowPaths ++ [
  pkgs.bash
  pkgs.git
  pkgs.curl
  pkgs.gawk
  pkgs.docker
  pkgs.docker-compose
  pkgs.which
  pkgs.coreutils-full
  pkgs.unixtools.ping
  pkgs.iptables
  pkgs.openssl
];

configureMailCow = pkgs.writeShellScriptBin "configureMailCow" ''
  ${pkgs.gnused}/bin/sed -i 's/HTTP_PORT=.*/HTTP_PORT=${httpPort}/' ${stateDir}/mailcow.conf
  ${pkgs.gnused}/bin/sed -i 's/HTTP_BIND=.*/HTTP_BIND=${httpBind}/' ${stateDir}/mailcow.conf
  ${pkgs.gnused}/bin/sed -i 's/HTTPS_PORT=.*/HTTPS_PORT=${httpsPort}/' ${stateDir}/mailcow.conf
  ${pkgs.gnused}/bin/sed -i 's/HTTPS_BIND=.*/HTTPS_BIND=${httpsBind}/' ${stateDir}/mailcow.conf
  ${pkgs.gnused}/bin/sed -i 's/SKIP_LETS_ENCRYPT=.*/SKIP_LETS_ENCRYPT=${ if skipMailCowAcme then "y" else "n"}/' ${stateDir}/mailcow.conf
'';

updateMailCow = pkgs.writeShellScriptBin "updateMailCow" ''
  cd ${stateDir}
  ./update.sh --no-update-compose --force
'';

runMailCow = pkgs.writeShellScriptBin "runMailCow" ''
  if [ -d "${stateDir}" ]
  then
    cd ${stateDir}
    ${configureMailCow}/bin/configureMailCow
    ${pkgs.docker-compose}/bin/docker-compose up
  else
    git clone ${gitUrl} "${stateDir}"
    cd "${stateDir}"
    git checkout ${gitRef}
    git reset --hard
    export MAILCOW_HOSTNAME="${ptr}"
    export MAILCOW_TZ="Europe/Berlin"
    ./generate_config.sh
    ${configureMailCow}/bin/configureMailCow
    ./update.sh --no-update-compose --force
  fi
'';

in
{
  runMailCow = {
    command = runMailCow;
    paths = runMailCowPaths;
  };

  configureMailCow = {
    command = configureMailCow;
    paths = configureMailCowPaths;
  };

  updateMailCow = {
    command = updateMailCow;
    paths = runMailCowPaths;
  };

  systemd = {
    services = {
      mailcow = {
        path = runMailCowPaths;
        serviceConfig.Type = "oneshot";
        wantedBy = ["multi-user.target"];
        after = [ "docker-service"];
        script = ''
          ${runMailCow}/bin/runMailCow
        '';
      };
      mailcow-update = {
        path = runMailCowPaths;
        serviceConfig.Type = "oneshot";
        script = ''
          ${updateMailCow}/bin/updateMailCow
        '';
      };
    };
    timers = {
      mailcow-update = {
        wantedBy = [ "timers.target" ];
        partOf = [ "mailcow-update.service" ];
        timerConfig.OnCalendar = "weekly";
      };
    };
  };

  nginx = {
      virtualHosts = {
        mailCow =  {
          enableACME = nginxEnableACME;
          forceSSL = nginxForceSSL;
          default = nginxIsDefault;
          locations."/".proxyPass = "http://localhost:${httpPort}";
          serverName = "${ptr}";
          serverAliases = nginxServerAliases;
      };
    };
  };

  acme = {
    certName = "${ptr}";
    postRun =''
      cp fullchain.pem ${stateDir}/data/assets/ssl/cert.pem
      cp key.pem ${stateDir}/data/assets/ssl/key.pem
      cp chain.pem ${stateDir}/data/assets/ssl/chain.pem
    '';
  };

  meta = {
    description = "The mailserver suite with the 'moo' ";
    homepage = https://mailcow.email;
    maintainers = "stubbfel";
    license = pkgs.lib.licenses.gpl3;
    platforms = pkgs.lib.platforms.unix;
  };
}